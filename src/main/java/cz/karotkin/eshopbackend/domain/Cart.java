package cz.karotkin.eshopbackend.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(name="id_gen", sequenceName="cart_seq", allocationSize=1)
@Getter
@Setter
public class Cart extends AbstractEntity {
    @ManyToMany
    @JoinTable(
            name = "r_cart_product",
            joinColumns = @JoinColumn(name = "cart_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> products;
}
