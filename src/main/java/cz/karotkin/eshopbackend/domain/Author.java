package cz.karotkin.eshopbackend.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@SequenceGenerator(name="id_gen", sequenceName="author_seq", allocationSize=1)
@Getter
@Setter
@NoArgsConstructor
public class Author extends AbstractEntity {
    private String name;
    private String bio;
    private LocalDate birthDate;
}
