package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.OrderDto;
import cz.karotkin.eshopbackend.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/cart/{cartId}")
    public OrderDto createOrder(@PathVariable("cartId") Long cartId) {
        return orderService.createOrder(cartId);
    }

    @GetMapping
    public List<OrderDto> findAllOrders() {
        return orderService.findAllOrders();
    }

    @PutMapping("/{orderId}/status/{status}")
    public OrderDto updateOrderStatus(@PathVariable("orderId") Long orderId, @PathVariable("status") String status) {
        return orderService.updateOrderStatus(orderId, status);
    }
}
