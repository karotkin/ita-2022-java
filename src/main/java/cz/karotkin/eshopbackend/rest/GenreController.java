package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.GenreDto;
import cz.karotkin.eshopbackend.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("api/v1/genres")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    @GetMapping
    public Collection<GenreDto> findAllGenres() {
        return genreService.findAllGenres();
    }
}
