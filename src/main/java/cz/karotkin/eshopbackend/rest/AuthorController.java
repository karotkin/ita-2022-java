package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.AuthorDto;
import cz.karotkin.eshopbackend.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("api/v1/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping
    public Collection<AuthorDto> findAllAuthors() {
        return authorService.findAllAuthors();
    }
}
