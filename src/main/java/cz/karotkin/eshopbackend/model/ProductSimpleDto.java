package cz.karotkin.eshopbackend.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductSimpleDto {

    private Long id;
    private String name;
    private Long price;
    private String image;

}
