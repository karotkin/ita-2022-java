package cz.karotkin.eshopbackend.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GenreDto {

    private Long id;
    private String name;
    private String description;

}