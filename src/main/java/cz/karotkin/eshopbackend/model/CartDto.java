package cz.karotkin.eshopbackend.model;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CartDto {

    private Long id;
    private List<ProductSimpleDto> products;

}
