package cz.karotkin.eshopbackend.model;

import cz.karotkin.eshopbackend.validation.StartsWithUppercase;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductRequestDto {

    @NotBlank
    @Size(max = 256)
    @StartsWithUppercase
    private String name;

    @NotBlank
    @Size(max = 512)
    private String description;

    @NotNull
    @Min(1)
    private Long price;

    @NotNull
    @Min(0)
    private Long stock;

    @NotBlank
    private String image;

    @NotNull
    private Long authorId;

    @NotNull
    private Long genreId;
}
