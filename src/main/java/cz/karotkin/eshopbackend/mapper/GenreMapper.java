package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Genre;
import cz.karotkin.eshopbackend.model.GenreDto;
import org.mapstruct.Mapper;

@Mapper
public interface GenreMapper {
    GenreDto toDto(Genre genre);
}
