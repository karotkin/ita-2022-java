package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.model.CartDto;
import org.mapstruct.Mapper;

@Mapper(uses = ProductMapper.class)
public interface CartMapper {
    CartDto toDto(Cart cart);
}
