package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Author;
import cz.karotkin.eshopbackend.model.AuthorDto;
import org.mapstruct.Mapper;

@Mapper
public interface AuthorMapper {
    AuthorDto toDto(Author author);
}
