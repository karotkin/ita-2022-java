package cz.karotkin.eshopbackend.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("warehouse")
public interface WarehouseClient {

    @GetMapping("api/v1/stocks/{productId}")
    Long getStock(@PathVariable Long productId);
}
