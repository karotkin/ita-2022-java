package cz.karotkin.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class FileNotReadableException extends ItaException {
    public FileNotReadableException() {
        super("File not readable", "0000", HttpStatus.BAD_REQUEST);
    }
}
