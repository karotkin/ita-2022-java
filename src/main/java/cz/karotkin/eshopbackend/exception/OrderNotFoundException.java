package cz.karotkin.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class OrderNotFoundException extends ItaException {
    public OrderNotFoundException(Long orderId) {
        super(String.format("Order %d not found!", orderId), "0001", HttpStatus.NOT_FOUND);
    }
}
