package cz.karotkin.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class CartNotFoundException extends ItaException {
    public CartNotFoundException(Long cartId) {
        super(String.format("Cart %d not found!", cartId), "0001", HttpStatus.NOT_FOUND);
    }
}
