package cz.karotkin.eshopbackend.service;

import cz.karotkin.eshopbackend.model.PreviewResponse;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * This service layer is responsible for CRUD operations above the products stored in the database.
 */
public interface ProductService {

    /**
     * The method finds all products.
     * @return products as a list of {@link ProductSimpleDto}
     */
    List<ProductSimpleDto> findAllProducts();

    /**
     * The method finds the product by its id.
     * @param id - id of the product
     * @return product as {@link ProductDto}
     */
    ProductDto findProduct(Long id);

    /**
     * The method creates a product.
     * @param productRequestDto - all fields represented by {@link ProductRequestDto}
     * @return new product as {@link ProductDto}
     */
    ProductDto createProduct(ProductRequestDto productRequestDto);

    /**
     * The method update a product found by its id.
     * @param id - id of the product
     * @param editProductDto - all fields represented by {@link ProductRequestDto}
     * @return updated product as {@link ProductDto}
     */
    ProductDto editProduct(Long id, ProductRequestDto editProductDto);

    /**
     * The method deletes a product found by its id.
     * @param id - id of the product
     */
    void deleteProduct(Long id);

    void addPreview(Long id, MultipartFile file);

    PreviewResponse getPreview(Long id);

    void updateStock();
}
