package cz.karotkin.eshopbackend.service;

import cz.karotkin.eshopbackend.model.GenreDto;

import java.util.List;

/**
 * This service layer is responsible for CRUD operations above the genres stored in the database.
 */
public interface GenreService {

    /**
     * The method finds all genres.
     * @return genres as a list of {@link GenreDto}
     */
    List<GenreDto> findAllGenres();
}
