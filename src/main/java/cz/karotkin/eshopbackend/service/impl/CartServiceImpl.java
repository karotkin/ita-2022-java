package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.exception.CartNotFoundException;
import cz.karotkin.eshopbackend.exception.ProductNotFoundException;
import cz.karotkin.eshopbackend.mapper.CartMapper;
import cz.karotkin.eshopbackend.model.CartDto;
import cz.karotkin.eshopbackend.repository.CartRepository;
import cz.karotkin.eshopbackend.repository.ProductRepository;
import cz.karotkin.eshopbackend.service.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final CartMapper cartMapper;

    @Override
    @Transactional
    public CartDto createCart(Long productId) {
        log.info("Creating cart with product id " + productId);

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));

        Cart cart = new Cart()
                .setProducts(List.of(product));
        cartRepository.save(cart);
        CartDto result = cartMapper.toDto(cart);
        log.debug("Created cart: {}", result);
        return result;
    }

    @Override
    @Transactional
    public CartDto updateCart(Long cartId, Long productId) {
        log.info("Updating cart with id " + cartId);

        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new CartNotFoundException(cartId));
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));

        cart.getProducts().add(product);
        CartDto result = cartMapper.toDto(cart);
        log.debug("Updated cart: {}", result);
        return result;
    }

    @Override
    @Transactional
    public void removeUnusedCarts() {
        List<Cart> unusedCarts = cartRepository.findAllByModifiedAtBefore(Instant.now().minus(10, ChronoUnit.MINUTES));
        if(unusedCarts.size() > 0) {
            cartRepository.deleteAll(unusedCarts);
            List<Long> idList = new ArrayList<>();
            for(Cart cart : unusedCarts) {
                idList.add(cart.getId());
            }
            log.debug("ID's of deleted carts: " + idList);
        }
    }
}
