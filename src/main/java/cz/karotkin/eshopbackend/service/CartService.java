package cz.karotkin.eshopbackend.service;

import cz.karotkin.eshopbackend.job.CartRemovingJob;
import cz.karotkin.eshopbackend.model.CartDto;

/**
 * This service layer is responsible for CRUD operations above the carts stored in the database.
 */
public interface CartService {

    /**
     * The method creates a cart.
     * @param productId - id of the product we want to add to the new cart
     * @return new cart as {@link CartDto}
     */
    CartDto createCart(Long productId);

    /**
     * The method updates a cart.
     * @param cartId - id of the cart we want to update
     * @param productId - id of the product we want to add to the cart
     * @return updated cart as {@link CartDto}
     */
    CartDto updateCart(Long cartId, Long productId);

    /**
     * The method is a job for checking carts and removing the unused ones automatically.
     * The job is defined here: {@link CartRemovingJob}
     */
    void removeUnusedCarts();
}