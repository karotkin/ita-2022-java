package cz.karotkin.eshopbackend.service;

import cz.karotkin.eshopbackend.model.OrderDto;

import java.util.List;

/**
 * This service layer is responsible for CRUD operations above the orders stored in the database.
 */
public interface OrderService {

    /**
     * The method creates an order.
     * @param cartId - id of the cart from which the order is created
     * @return new order as {@link OrderDto}
     */
    OrderDto createOrder(Long cartId);

    /**
     * The method finds all orders.
     * @return orders as a list of {@link OrderDto}
     */
    List<OrderDto> findAllOrders();

    /**
     * The method updates the status of an order.
     * @param orderId - id of the order
     * @param status - new value of the status
     * @return updated order as {@link OrderDto}
     */
    OrderDto updateOrderStatus(Long orderId, String status);
}
