package cz.karotkin.eshopbackend.service;

import cz.karotkin.eshopbackend.model.AuthorDto;

import java.util.List;

/**
 * This service layer is responsible for CRUD operations above the authors stored in the database.
 */
public interface AuthorService {

    /**
     * The method finds all authors.
     * @return authors as a list of {@link AuthorDto}
     */
    List<AuthorDto> findAllAuthors();
}
