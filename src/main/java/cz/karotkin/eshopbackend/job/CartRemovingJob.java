package cz.karotkin.eshopbackend.job;

import cz.karotkin.eshopbackend.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CartRemovingJob {

    @Autowired
    private CartService cartService;

    @Scheduled(cron = "${app.job.unused-carts-removal.cron}")
    public void removeUnusedCarts() {
        log.debug("Checking for unused carts.");
        cartService.removeUnusedCarts();
    }
}
