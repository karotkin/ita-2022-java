package cz.karotkin.eshopbackend.ws;

import cz.ares.response.VypisRZP;

public interface AresClient {
    VypisRZP getCompanyInfo(String ico);
}
