package cz.karotkin.eshopbackend.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidation implements ConstraintValidator<StartsWithUppercase, String> {
    @Override
    public void initialize(StartsWithUppercase constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        if(name != null && name.trim().length() > 0) {
            return name.charAt(0) == name.toUpperCase().charAt(0);
        }
        return false;
    }
}
