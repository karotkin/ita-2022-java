package cz.karotkin.eshopbackend.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NameValidation.class)
public @interface StartsWithUppercase {
    String message() default "{validation.validName}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
