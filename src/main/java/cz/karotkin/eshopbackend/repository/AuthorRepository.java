package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
