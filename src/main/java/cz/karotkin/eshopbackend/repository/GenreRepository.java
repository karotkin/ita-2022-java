package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}
