package cz.karotkin.eshopbackend.mother;

import cz.karotkin.eshopbackend.domain.Author;
import cz.karotkin.eshopbackend.model.AuthorDto;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public class AuthorMother {

    public static Author prepareAuthor() {
        return (Author) new Author()
                .setBio("Narozen v Písku")
                .setBirthDate(LocalDate.of(1986, Month.APRIL, 13))
                .setName("Dominik Bican")
                .setId(1L);
    }

    public static AuthorDto prepareAuthorDto() {
        return new AuthorDto()
                .setBio("Narozen v Písku")
                .setBirthDate(LocalDate.of(1986, Month.APRIL, 13))
                .setName("Dominik Bican")
                .setId(1L);
    }

    public static List<Author> prepareAuthors() {
        List<Author> authors = List.of(prepareAuthor(), prepareAuthor());
        return authors;
    }

    public static List<AuthorDto> prepareAuthorsDto() {
        List<AuthorDto> authorsDto = List.of(prepareAuthorDto(), prepareAuthorDto());
        return authorsDto;
    }

}
