package cz.karotkin.eshopbackend.mother;

import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;

import java.util.List;

import static cz.karotkin.eshopbackend.mother.AuthorMother.prepareAuthor;
import static cz.karotkin.eshopbackend.mother.AuthorMother.prepareAuthorDto;
import static cz.karotkin.eshopbackend.mother.GenreMother.prepareGenre;
import static cz.karotkin.eshopbackend.mother.GenreMother.prepareGenreDto;

public class ProductMother {

    public static Product prepareProduct() {
        return new Product()
                .setName("Testovací kniha")
                .setDescription("Kniha o testování")
                .setPrice(1000L)
                .setStock(50L)
                .setImage("xxx")
                .setAuthor(prepareAuthor())
                .setGenre(prepareGenre());
    }

    public static ProductDto prepareProductDto() {
        return new ProductDto()
                .setId(2L)
                .setName("Testovací kniha")
                .setDescription("Kniha o testování")
                .setPrice(1000L)
                .setStock(50L)
                .setImage("xxx")
                .setAuthor(prepareAuthorDto())
                .setGenre(prepareGenreDto());
    }

    public static ProductSimpleDto prepareProductSimpleDto() {
        return new ProductSimpleDto()
                .setId(2L)
                .setName("Testovací kniha")
                .setPrice(1000L)
                .setImage("xxx");
    }

    public static ProductRequestDto prepareProductRequestDto() {
        return new ProductRequestDto()
                .setName("Testovací kniha")
                .setDescription("Kniha o testování")
                .setPrice(1000L)
                .setStock(50L)
                .setImage("xxx")
                .setAuthorId(1L)
                .setGenreId(1L);
    }

    public static List<Product> prepareProducts() {
        List<Product> products = List.of(prepareProduct(), prepareProduct());
        return products;
    }

    public static List<ProductSimpleDto> prepareProductsSimpleDto() {
        List<ProductSimpleDto> productsSimpleDto = List.of(prepareProductSimpleDto(), prepareProductSimpleDto());
        return productsSimpleDto;
    }
}
