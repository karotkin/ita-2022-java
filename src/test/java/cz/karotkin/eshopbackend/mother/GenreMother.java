package cz.karotkin.eshopbackend.mother;

import cz.karotkin.eshopbackend.domain.Genre;
import cz.karotkin.eshopbackend.model.GenreDto;

import java.util.List;

public class GenreMother {

    public static Genre prepareGenre() {
        return (Genre) new Genre()
                .setName("Detektivka")
                .setDescription("Detektivní žánr")
                .setId(1L);
    }

    public static GenreDto prepareGenreDto() {
        return new GenreDto()
                .setName("Detektivka")
                .setDescription("Detektivní žánr")
                .setId(1L);
    }

    public static List<Genre> prepareGenres() {
        List<Genre> genres = List.of(prepareGenre(), prepareGenre());
        return genres;
    }

    public static List<GenreDto> prepareGenresDto() {
        List<GenreDto> genresDto = List.of(prepareGenreDto(), prepareGenreDto());
        return genresDto;
    }
}
