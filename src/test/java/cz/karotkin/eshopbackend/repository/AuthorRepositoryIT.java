package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Author;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;

@DataJpaTest
class AuthorRepositoryIT implements WithAssertions {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void createAndRetrieveAuthor() {
        Author author = new Author()
                .setName("Dominik Bican")
                .setBio("Narozen v Písku")
                .setBirthDate(LocalDate.of(1986, Month.APRIL, 13));

        testEntityManager.persistAndFlush(author);
        testEntityManager.detach(author);

        Optional<Author> result = authorRepository.findById(author.getId());

        assertThat(result).isNotEmpty();

        Author resultAuthor = result.get();

        assertThat(resultAuthor).usingRecursiveComparison().isEqualTo(author);
    }

}