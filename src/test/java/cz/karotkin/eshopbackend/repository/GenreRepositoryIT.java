package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Genre;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

@DataJpaTest
class GenreRepositoryIT implements WithAssertions {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void createAndRetrieveGenre() {

        Genre genre = new Genre()
                .setName("Detektivka")
                .setDescription("Vražedné motivy");

        testEntityManager.persistAndFlush(genre);
        testEntityManager.detach(genre);

        Optional<Genre> result = genreRepository.findById(genre.getId());

        assertThat(result).isNotEmpty();

        Genre resultGenre = result.get();

        assertThat(resultGenre).usingRecursiveComparison().isEqualTo(genre);

    }

}