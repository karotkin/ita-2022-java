package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static cz.karotkin.eshopbackend.mother.ProductMother.prepareProduct;
import static cz.karotkin.eshopbackend.mother.ProductMother.prepareProductRequestDto;

@SpringBootTest(classes = {ProductMapperImpl.class, AuthorMapperImpl.class, GenreMapperImpl.class})
class ProductMapperTest implements WithAssertions {

    @Autowired
    private ProductMapper productMapper;


    @Test
    void toDomain() {
        ProductRequestDto productDto = prepareProductRequestDto();
        Product result = productMapper.toDomain(productDto);

        assertThat(result.getName()).isEqualTo(productDto.getName());
        assertThat(result.getDescription()).isEqualTo(productDto.getDescription());
        assertThat(result.getPrice()).isEqualTo(productDto.getPrice());
        assertThat(result.getStock()).isEqualTo(productDto.getStock());
        assertThat(result.getImage()).isEqualTo(productDto.getImage());
    }

    @Test
    void toDto() {
        Product product = prepareProduct();
        ProductDto result = productMapper.toDto(product);

        assertThat(result.getId()).isEqualTo(product.getId());
        assertThat(result.getName()).isEqualTo(product.getName());
        assertThat(result.getDescription()).isEqualTo(product.getDescription());
        assertThat(result.getPrice()).isEqualTo(product.getPrice());
        assertThat(result.getStock()).isEqualTo(product.getStock());
        assertThat(result.getImage()).isEqualTo(product.getImage());
        assertThat(result.getGenre()).isNotNull();
        assertThat(result.getAuthor()).isNotNull();
    }

    @Test
    void toSimpleDto() {
        Product product = prepareProduct();
        ProductSimpleDto result = productMapper.toSimpleDto(product);

        assertThat(result.getId()).isEqualTo(product.getId());
        assertThat(result.getName()).isEqualTo(product.getName());
        assertThat(result.getPrice()).isEqualTo(product.getPrice());
        assertThat(result.getImage()).isEqualTo(product.getImage());
    }

    @Test
    void mergeProduct() {
        ProductRequestDto productDto = prepareProductRequestDto();
        Product product = prepareProduct();
        productMapper.mergeProduct(product, productDto);

        assertThat(product.getName()).isEqualTo(productDto.getName());
        assertThat(product.getDescription()).isEqualTo(productDto.getDescription());
        assertThat(product.getPrice()).isEqualTo(productDto.getPrice());
        assertThat(product.getStock()).isEqualTo(productDto.getStock());
        assertThat(product.getImage()).isEqualTo(productDto.getImage());
    }
}