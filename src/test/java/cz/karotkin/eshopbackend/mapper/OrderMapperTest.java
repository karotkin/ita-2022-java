package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Order;
import cz.karotkin.eshopbackend.model.OrderDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static cz.karotkin.eshopbackend.mother.OrderMother.prepareOrder;

class OrderMapperTest implements WithAssertions {

    private final OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);

    @Test
    void toDto() {
        Order order = prepareOrder();
        OrderDto result = orderMapper.toDto(order);

        assertThat(result.getId()).isEqualTo(order.getId());
        assertThat(result.getProducts()).isEqualTo(order.getProducts());

    }
}