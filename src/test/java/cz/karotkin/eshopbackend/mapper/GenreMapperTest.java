package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Genre;
import cz.karotkin.eshopbackend.model.GenreDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static cz.karotkin.eshopbackend.mother.GenreMother.prepareGenre;

class GenreMapperTest implements WithAssertions {

    private final GenreMapper genreMapper = Mappers.getMapper(GenreMapper.class);

    @Test
    void toDto() {
        Genre genre = prepareGenre();
        GenreDto result = genreMapper.toDto(genre);

        assertThat(result.getId()).isEqualTo(genre.getId());
        assertThat(result.getName()).isEqualTo(genre.getName());
        assertThat(result.getDescription()).isEqualTo(genre.getDescription());
    }
}