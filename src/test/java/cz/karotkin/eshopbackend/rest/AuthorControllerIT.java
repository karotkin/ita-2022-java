package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.AuthorDto;
import cz.karotkin.eshopbackend.repository.AuthorRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthorControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    void findAllAuthors() {

        ResponseEntity<AuthorDto[]> response = testRestTemplate.getForEntity("/api/v1/authors", AuthorDto[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        AuthorDto[] body = response.getBody();

        assertThat(body).hasSize(2);
    }
}