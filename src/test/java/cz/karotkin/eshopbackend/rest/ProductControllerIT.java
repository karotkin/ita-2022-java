package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.repository.AuthorRepository;
import cz.karotkin.eshopbackend.repository.GenreRepository;
import cz.karotkin.eshopbackend.repository.ProductRepository;
import org.assertj.core.api.WithAssertions;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import static cz.karotkin.eshopbackend.mother.ProductMother.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Test
    void findProduct() {

        Product product = prepareProduct();

        productRepository.save(product);

        ResponseEntity<ProductDto> response = testRestTemplate.getForEntity("/api/v1/products/" + product.getId(), ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ProductDto body = response.getBody();

        assertThat(body).isNotNull();
        assertThat(body).usingRecursiveComparison(RecursiveComparisonConfiguration.builder()
                .withIgnoredFieldsMatchingRegexes("id")
                .build()).isEqualTo(product);

        productRepository.deleteById(product.getId());
    }

    @Test
    void createProduct() {

        ProductRequestDto productRequestDto = prepareProductRequestDto();
        ProductDto productDto = prepareProductDto();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic a2Fyb3RraW46cGFzc3dvcmQ=");
        HttpEntity<ProductRequestDto> request = new HttpEntity<>(productRequestDto, httpHeaders);

        ResponseEntity<ProductDto> response = testRestTemplate.postForEntity("/api/v1/products", request, ProductDto.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ProductDto body = response.getBody();

        assertThat(body).isNotNull();
        assertThat(body).usingRecursiveComparison(RecursiveComparisonConfiguration.builder()
                .withIgnoredFieldsMatchingRegexes("id")
                .build()).isEqualTo(productDto);

        productRepository.deleteById(body.getId());

    }

    @Test
    void deleteProduct() {

        Product product = prepareProduct();

        productRepository.save(product);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic a2Fyb3RraW46cGFzc3dvcmQ=");
        HttpEntity<Object> request = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> response = testRestTemplate.exchange("/api/v1/products/" + product.getId(), HttpMethod.DELETE, request, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}