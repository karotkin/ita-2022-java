package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Genre;
import cz.karotkin.eshopbackend.mapper.GenreMapper;
import cz.karotkin.eshopbackend.model.GenreDto;
import cz.karotkin.eshopbackend.repository.GenreRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static cz.karotkin.eshopbackend.mother.GenreMother.prepareGenres;
import static cz.karotkin.eshopbackend.mother.GenreMother.prepareGenresDto;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GenreServiceImplTest implements WithAssertions {

    @InjectMocks
    private GenreServiceImpl genreService;

    @Mock
    private GenreRepository genreRepository;

    @Mock
    private GenreMapper genreMapper;

    @Test
    void findAllGenres() {
        List<Genre> genres = prepareGenres();
        List<GenreDto> expectedResult = prepareGenresDto();

        when(genreRepository.findAll())
                .thenReturn(genres);
        when(genreMapper.toDto(genres.get(0)))
                .thenReturn(expectedResult.get(0));
        when(genreMapper.toDto(genres.get(1)))
                .thenReturn(expectedResult.get(1));

        List<GenreDto> result = genreService.findAllGenres();

        assertThat(result).isEqualTo(expectedResult);
        verify(genreRepository).findAll();
        verify(genreMapper).toDto(genres.get(0));
        verify(genreMapper).toDto(genres.get(1));
    }
}