package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.domain.Order;
import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.exception.CartNotFoundException;
import cz.karotkin.eshopbackend.mapper.OrderMapper;
import cz.karotkin.eshopbackend.model.OrderDto;
import cz.karotkin.eshopbackend.model.OrderStatus;
import cz.karotkin.eshopbackend.repository.CartRepository;
import cz.karotkin.eshopbackend.repository.OrderRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static cz.karotkin.eshopbackend.mother.CartMother.prepareCart;
import static cz.karotkin.eshopbackend.mother.OrderMother.prepareOrderDto;
import static cz.karotkin.eshopbackend.mother.ProductMother.prepareProduct;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest implements WithAssertions {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private OrderMapper orderMapper;

    @Captor
    private ArgumentCaptor<Order> orderCaptor;

    @Test
    void createOrder() {
        Product product = prepareProduct();
        Cart cart = prepareCart().setProducts(List.of(product));
        OrderDto expectedResult = prepareOrderDto();

        when(cartRepository.findById(1L))
                .thenReturn(Optional.of(cart));
        when(orderMapper.toDto(any()))
                .thenReturn(expectedResult);

        OrderDto result = orderService.createOrder(1L);

        assertThat(expectedResult).isEqualTo(result);
        verify(cartRepository).findById(1L);
        verify(orderRepository).save(orderCaptor.capture());

        Order capturedOrder = orderCaptor.getValue();
        assertThat(capturedOrder.getProducts()).contains(product);
        assertThat(capturedOrder.getStatus()).isEqualTo(OrderStatus.NEW);
    }

    @Test
    void createOrder_CartNotFound() {
        when(cartRepository.findById(1L))
                .thenReturn(Optional.empty());
        assertThrows(CartNotFoundException.class, () -> {
            orderService.createOrder(1L);
        });
        verifyNoInteractions(orderMapper, orderRepository);
    }
}