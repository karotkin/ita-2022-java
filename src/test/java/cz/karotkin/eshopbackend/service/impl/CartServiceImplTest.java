package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.exception.CartNotFoundException;
import cz.karotkin.eshopbackend.exception.ProductNotFoundException;
import cz.karotkin.eshopbackend.mapper.CartMapper;
import cz.karotkin.eshopbackend.model.CartDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import cz.karotkin.eshopbackend.repository.CartRepository;
import cz.karotkin.eshopbackend.repository.ProductRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cz.karotkin.eshopbackend.mother.CartMother.prepareCart;
import static cz.karotkin.eshopbackend.mother.CartMother.prepareCartDto;
import static cz.karotkin.eshopbackend.mother.ProductMother.prepareProduct;
import static cz.karotkin.eshopbackend.mother.ProductMother.prepareProductSimpleDto;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest implements WithAssertions {

    @InjectMocks
    private CartServiceImpl cartService;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private CartMapper cartMapper;

    @Mock
    private ProductRepository productRepository;

    @Captor
    private ArgumentCaptor<Cart> cartCaptor;

    @Test
    void createCart() {
        Product product = prepareProduct();
        ProductSimpleDto productSimpleDto = prepareProductSimpleDto();
        CartDto expectedResult = prepareCartDto().setProducts(List.of(productSimpleDto));

        when(productRepository.findById(1L))
                .thenReturn(Optional.of(product));
        when(cartMapper.toDto(any()))
                .thenReturn(expectedResult);

        CartDto result = cartService.createCart(1L);

        assertThat(expectedResult).isEqualTo(result);
        verify(productRepository).findById(1L);
        verify(cartMapper).toDto(cartCaptor.capture());
        verify(cartRepository).save(cartCaptor.capture());
        assertThat(cartCaptor.getValue().getProducts()).contains(product);
    }

    @Test
    void createCart_productNotFound() {
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            cartService.createCart(1L);
        });
        verifyNoInteractions(cartMapper, cartRepository);
    }

    @Test
    void updateCart() {
        Product product = prepareProduct();
        Cart cart = prepareCart().setProducts(new ArrayList<>());
        CartDto expectedResult = prepareCartDto();

        when(productRepository.findById(1L))
                .thenReturn(Optional.of(product));
        when(cartRepository.findById(1L))
                .thenReturn(Optional.of(cart));
        when(cartMapper.toDto(cart))
                .thenReturn(expectedResult);

        CartDto result = cartService.updateCart(1L, 1L);

        assertThat(expectedResult).isEqualTo(result);
        verify(productRepository).findById(1L);
        verify(cartRepository).findById(1L);
        verify(cartMapper).toDto(cart);
    }

    @Test
    void updateCart_productNotFound() {
        when(cartRepository.findById(1L))
                .thenReturn(Optional.of(prepareCart()));
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            cartService.updateCart(1L, 1L);
        });
        verifyNoInteractions(cartMapper);
    }

    @Test
    void updateCart_cartNotFound() {
        when(cartRepository.findById(1L))
                .thenReturn(Optional.empty());
        assertThrows(CartNotFoundException.class, () -> {
            cartService.updateCart(1L, 1L);
        });
        verifyNoInteractions(cartMapper, productRepository);
    }
}