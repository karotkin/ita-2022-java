# Eshop backend

## About

This application is a backend solution of a fictitious eshop for books.
It stores in a database our entities like products (books), authors, genres, etc.

## Technologies used in the app

The app is completely written in Spring Boot (2.6.8).
For storing our entities we use the PostgreSQL database.
We set a Logback for separate logs in 2 loggers (Spring Boot into the console and our services into the file).
Our app is connected to S3 storage in AWS.
It is also prepared to be used as a microservice with registration in Eureka. We also use a config server to store the property files
of all the microservices.
We implemented the Spring Security for the basic security.

## Getting started

It is highly recommended to use Java 17 or higher.  
For creating your local repository: `git clone https://gitlab.com/karotkin/ita-2022-java.git`  
To start the database, run the docker image: `docker-compose up`  
It is a Maven project, you need to run this command: `mvn clean install`  
To start the application, use this command: `java -jar target/eshop-backend-0.0.1-SNAPSHOT.jar`
